(function(MuEngine){
	'use strict';

	if(!MuEngine){ MuEngine = window.MuEngine = {};}
	/**
 * set the current canvas where the engine will draw.
 * it will init both MuEngine.canvas and MuEngine.ctx module attributes.
 * @param useDoubleBuffer
	* creates a internal dupplicate of activeCanvas and redirect ctx to it.
	* render operations will happen in backbuffer and you will need to call
	* MuEngine.swapBackbuffer to copy into active canvas.
 */
	MuEngine.setActiveCanvas = function(canvas, useDoubleBuffer){
		MuEngine.canvas = canvas;
		if(useDoubleBuffer){
			MuEngine.buffer = document.createElement("canvas");
			MuEngine.buffer.width = MuEngine.canvas.width;
			MuEngine.buffer.height = MuEngine.canvas.height;
			MuEngine.ctx = MuEngine.buffer.getContext('2d');
		}
		else
			MuEngine.ctx = MuEngine.canvas.getContext('2d');
	};

	/**
	* render the doublebuffer canvas into the active canvas
	*/
	MuEngine.swapBackBuffer = function(){
		MuEngine.canvas.getContext('2d').drawImage(MuEngine.buffer, 0, 0);
	};


	/**
 * set the current grid to be rendered by the engine
 */
	MuEngine.setActiveGrid = function(grid){
		MuEngine.grid = grid;
	};

	/**
 * set the active camera to be used to render the world
 */
	MuEngine.setActiveCamera = function(camera){
		MuEngine.camera = camera;
	};


	/**
* clear the current canvas
*/
	MuEngine.clear = function(){
		MuEngine.ctx.clearRect(0, 0, MuEngine.canvas.width, MuEngine.canvas.height);
	};

	/**
 * set the target fps for running the engine
 */
	MuEngine.setTargetFps = function(fps){
		MuEngine.fps = fps;
	};

	/**
 * starts the gameloop with the given fps.
 * @see MuEngine.setTargetFps to change fps,
 * @see MuEngine.stop to terminate the loop.
 */
	MuEngine.start = function(){
		if(this.MuEngine.interval) return false;
		this.MuEngine.interval = setInterval(tick, 1000.0 / this.MuEngine.fps);
		this.MuEngine.start_time = Date.now();
		return true;
	};

	/**
 * stops the game loop (if it is running) and clear the interval
 */
	MuEngine.stop = function(){
		if(!this.MuEngine.interval) return false;
		clearInterval(this.MuEngine.interval);
		this.MuEngine.interval = null;
		return true;
	};

	/**
* load a image. it will return a MuEngineImageHandler that will allow to use a
* dummy image while the final one is being loaded. this will simplify the coding because
* the developer won't need to handle load callbacks
*/
	MuEngine.getImageHandler  = function(imgpath, sizex, sizey){
		//1. check if image has already been loaded
		if(MuEngine.imageHandlers[imgpath]){
			return MuEngine.imageHandlers[imgpath];
		}

		//2. check if default image is loaded, else, create it.
		if(!MuEngine.defimg){
			MuEngine.defimg = new Image();
			MuEngine.defimg.src = "data:image/gif;base64,R0lGODlhEAAOALMAAOazToeHh0tLS/7LZv/0jvb29t/f3//Ub//ge8WSLf/rhf/3kdbW1mxsbP//mf///yH5BAAAAAAALAAAAAAQAA4AAARe8L1Ekyky67QZ1hLnjM5UUde0ECwLJoExKcppV0aCcGCmTIHEIUEqjgaORCMxIC6e0CcguWw6aFjsVMkkIr7g77ZKPJjPZqIyd7sJAgVGoEGv2xsBxqNgYPj/gAwXEQA7";
		}
		//3. create an image handler with the default image, and start the loading of the real one.
		var handler = new MuEngine.ImageHandler(imgpath, sizex, sizey);
		MuEngine.imageHandlers[imgpath]= handler;
		return handler;
	};

	/**
 * render a node and all its children.
 * node is assumed to be the root of the world.
 * this method will generate recursive calls.
 */
	MuEngine.renderNode = function(node){
		//@todo: move this to private module attributes
		var mat = mat4.create();
		node.render(mat);
	};

	/**
 * compute the elapsed time
 */
	MuEngine.tick = function(){
		var now = Date.now();
		var dt = now - MuEngine.start_time;
		MuEngine.start_time = now;
		return dt;
	};

	/**
* Similar to renderNode, but invokes update(dt) in each node.
*/
	MuEngine.updateNode = function(node, dt){
		node.update(dt);
	};


})(window.MuEngine);
