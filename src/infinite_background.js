(function(MuEngine){
    'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}

  //------- INFINITE_BACKGROUND CLASS -------------------


	/**
	 * InfiniteBackground is a primitive. but is special.
   * it is intended to provide the illusion of a infinite background in pure 2d-mode.
   * it works by attaching to the root node, then it will fill the screen with the given
   * background texture.
   * it will track the current camera position, and offset the background texture as needed
   * (internally, it means it may render as 4 times the background in order to fill the screen using the texture as tile)
   * IMPORTANT: Current version does not respond to camera.zoom changes! all other objects will zoom while the background will keep the same size.
   * @param background_texture_path {string} path to the texture to be used as background.
   */
	MuEngine.InfiniteBackground	= function(background_texture_path){

		this.imghandler = MuEngine.getImageHandler(background_texture_path,  MuEngine.canvas.width, MuEngine.canvas.height);
 	};

	/*
	 * renders the primitive (line)
	 * @param: node: The node this primitive belongs to
	 */
	MuEngine.InfiniteBackground.prototype.render = function(node, cam){

    var img = this.imghandler.img;

    //lets use  the node eye pos, and assume cam is at origin in screen coords.

    var w = MuEngine.canvas.width;
    var h = MuEngine.canvas.height;

    var w2 = w >> 1;
    var h2 = h >> 1;
    var orix = (node.ep[0] - w2) % w;
    var oriy = (node.ep[1] - h2) % h;

    if(orix < 0) orix += w;
    if(oriy < 0) oriy += h;

	//	MuEngine.clear();

		var w_orix = w-orix;
		var h_oriy = h-oriy;


    //bottom-right
		if(h_oriy > 0 && w_orix > 0)
		MuEngine.ctx.drawImage(img, 0, 0,  w_orix , h_oriy, 				orix,     oriy,     w_orix, h_oriy);
    //up-right
		if(oriy > 0 && w_orix > 0)
    MuEngine.ctx.drawImage(img, 0, h_oriy,  w_orix , oriy, 				orix,     0,     w_orix, oriy);
    //up-left
    if(orix > 0 && oriy > 0)
    MuEngine.ctx.drawImage(img, w_orix , h_oriy, orix, oriy,  		0, 0, orix, oriy);
    //bottom-left
    if(orix > 0 && h_oriy > 0)
    MuEngine.ctx.drawImage(img, w_orix , 0, orix, h_oriy,  		0, oriy, orix, h_oriy);

	};

	/*do nothing*/
	MuEngine.InfiniteBackground.prototype.update = function(node, dt){
	};

})(window.MuEngine);
