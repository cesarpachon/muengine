(function(MuEngine){
    'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}

  //------- CELL CLASS EXTENDS NODE ------------


/**
	 * helper sort function for the internal render in the cell
	 */
	var _compareNodesByEyePos = function(nodeA, nodeB){
    return nodeA.ep[2] < nodeB.ep[2]? 1 : nodeA.ep[2] > nodeB.ep[2]? -1 : 0;
	};

/*
 * this is a private class, not expected to be instantiated for the user
 */

MuEngine.Cell = function(i, j, cellsize){
	MuEngine.Node.call(this);
	this.row = i;
	this.col = j;
	//a vector to store eye coordinates. it is using for render sorting
	 MuEngine.Cell.cellsize = cellsize;

  //transform points to the center of the cell
  this.transform.setPos(i*MuEngine.Cell.cellsize + (MuEngine.Cell.cellsize*0.5), 0, j*MuEngine.Cell.cellsize + (MuEngine.Cell.cellsize*0.5));
	this.transform.update();

  //wcorners are the corners of the cell in world coorlds (local coords are to easy to compute, no sense to store them)
  //they are computed at start time and on demand (not on each cell update tick)
  this.wcorners = [
    vec3.create(),
    vec3.create(),
    vec3.create(),
    vec3.create()
  ];

  /**
   * binary field. if zero, means the cell is walkable (no blocking flags)
   * we expect this field to be used for types of terrain.. water, mud..
   * @type {number}
   */
  this.walkable = 0;
  /**
   * binary field. first four bits means a wall in a dir.
   * north: 0x1
   * south: 0x2
   * east: 0x4
   * west:0x8
   * @type {number}
   */
  this.walls = 0;
};

//chaining prototypes
MuEngine.Cell.prototype = new MuEngine.Node();

/*
* we store the cellsize at the prototype level to share it between cell instances.
* we dont expect to have grids with differents cellsizes at the same time!
 */
MuEngine.Cell.prototype.cellsize = 0;


MuEngine.Cell.prototype.setWalkable = function(flag){
  this.walkable = flag?0:1;
};

/**
 * may I enter to this cell (no matters the original direction?)
 * @returns {boolean}
 */
MuEngine.Cell.prototype.isWalkable = function(){
    if(this.walkable === 0){
      //no blocking flags!
      return true;
    }
    else{
     return false;
    }
};

/**
 * returns true if an avatar in the current cell can move out of the cell in the current direction,
 * this method only test for the existence of inner walls in the cell. it does not test for walls in the
 * next cell, nor if there is a next cell at all.
 * use it as a quick filter to reject invalid movements in a cheap way before testing for neighbors existence.
 * @param dir {string} direction "north", "south"..
 */
MuEngine.Cell.prototype.hasWall = function(dir){
  if(dir === "north"){
    return (this.walls & 0x1) > 0;
  }else if(dir === "south"){
    return (this.walls & 0x2) > 0;
  }else if(dir === "east"){
    return (this.walls & 0x4) > 0;
  }else if(dir === "west"){
    return (this.walls & 0x8) > 0;
  }
};

/**
 * set a wall in the given dir.
 * @todo: test all this stuff!
 * @param dir
 */
MuEngine.Cell.prototype.setWall = function(dir){
  if(dir === "north"){
    this.walls = this.walls | 0x1;
  }else if(dir === "south"){
    this.walls = this.walls | 0x2;
  }else if(dir === "east"){
    this.walls = this.walls | 0x4;
  }else if(dir === "west"){
    this.walls = this.walls | 0x8;
  }
};

/**
 * returns a new vec3 object with a random point contained within the cell limits.
 * @param absolute {boolean} a flag to indicate if the returned point must be in local or global coords
 * @param padding {float} a percentage (eg: 0.1 == 10%) to limit the area of valid random points by adding padding to
 * each edge. (all four edges will have 10% of padding)
 */
MuEngine.Cell.prototype.getRandomPos = function(absolute, padding){
    var p = vec3.create();
    var border = padding*MuEngine.Cell.cellsize;
    var valid = MuEngine.Cell.cellsize - (border+border);
	  p[0] = (absolute?this.wp[0]:0) + border + Math.random()*valid;
    p[1] = this.wp[1];
    p[2] = (absolute?this.wp[2]:0) + border + Math.random()*valid;
    return p;
};


/**
 * @override Node.render.
 * original method assume the node has a primitive before iterating over children (we not).
 * second, it invokes in each children the render method, that updates worldmat and render in each step.
 * here, we separate those steps in two different loops: first update the children matrixes,
 * then, compute eyepos, sort by eyepos and finally render.
 * @todo: if we know that a grid is going to stay static, we dont need to update world mats in each step!
 * @todo: also, if we know some sprites are static, we dont need to update them too.
 */
MuEngine.Cell.prototype.render = function(mat){
	var i;
  this.updateWorldMat(mat);
	for(i=0; i<this.children.length; ++i){
		this.children[i].updateWorldMat(this.wm);
  }
  this.children.sort(_compareNodesByEyePos);
 for(i=0; i<this.children.length; ++i){
		this.children[i].render(this.wm);
	}
};



MuEngine.Cell.prototype._updateCorners = function(node){
  var _s2 = MuEngine.Cell.cellsize * 0.5;
  //lower left corner:
  vec3.set(MuEngine.p0, this.transform.pos[0] - _s2, this.transform.pos[1], this.transform.pos[2] - _s2);
  node.multP(MuEngine.p0, this.wcorners[0]);

  //lower right corner:
  vec3.set(MuEngine.p0, this.transform.pos[0] + _s2, this.transform.pos[1], this.transform.pos[2] - _s2);
  node.multP(MuEngine.p0, this.wcorners[1]);

  //upper left corner:
  vec3.set(MuEngine.p0, this.transform.pos[0] + _s2, this.transform.pos[1], this.transform.pos[2] + _s2);
  node.multP(MuEngine.p0, this.wcorners[2]);

  //upper right corner:
  vec3.set(MuEngine.p0, this.transform.pos[0] - _s2, this.transform.pos[1], this.transform.pos[2] + _s2);
  node.multP(MuEngine.p0, this.wcorners[3]);

};

/**
* returns true if the given pixel coordinates are into this cell projected region
*/
MuEngine.Cell.prototype.collision= function(px, py, node, cam){

  //lower left corner:
  cam.project(this.wcorners[0], MuEngine.p0);
  //lower right corner:
  cam.project(this.wcorners[1], MuEngine.p1);
  //upper left corner:
  cam.project(this.wcorners[2], MuEngine.p2);
  //upper right corner:
  cam.project(this.wcorners[3], MuEngine.p3);

  /*console.log("cell.collision", px, py,
              MuEngine.p0[0], MuEngine.p0[1],
              MuEngine.p1[0],MuEngine.p1[1],
              MuEngine.p2[0],MuEngine.p2[1],
              MuEngine.p3[0],MuEngine.p3[1], this.transform);
  */
  return MuEngine.pointInOOBBox(px, py,
      MuEngine.p0[0],MuEngine.p0[1],
      MuEngine.p1[0],MuEngine.p1[1],
      MuEngine.p2[0],MuEngine.p2[1],
      MuEngine.p3[0],MuEngine.p3[1]);
};
})(window.MuEngine);
