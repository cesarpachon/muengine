(function(MuEngine){
  'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}

  /**
  * @constructor
  * Spline class implements a sequence of catmull-rom segments
  * @see SplineTrackerAnimator to make a node follow a spline.
  */
  MuEngine.Spline = function(points, sub, res){
    this.control = points;
    this.sub = sub;
    this.res = res;
    this.path = this.smooth(this.control, this.sub, this.res);
    this.normalize();

  };

  /**
  * return the length of the spline (addition of linear path segments)
  */
  MuEngine.Spline.prototype.len = function(){
    return this._len;
  };




  /**
  * @param points {Array} array of vec3 control points
  * @param sub {number} number of parts to split on each segment. it will produce sub-1 additional points by segment.
  * @param res {number} tolerance
  * @return {Array} smoothed points, expected size is max ((points.length-1)*sub) + 1;
  */
  MuEngine.Spline.prototype.smooth = function(points, sub, res){
    var p0, p1, p2, p3;
    var step = 1.0/sub;
    var path = [];
    this._len = 0.0; //compute lenght of spline
    for(var i=0; i<points.length-1; ++i){

      p0 = points[ Math.max( 0, i-1 ) ];
      p1 = points[ i ];
      p2 = points[ Math.min( i+1, points.length-1 ) ];
      p3 = points[ Math.min( i+2, points.length-1 ) ];

      if(i>0){
        this._len += vec3.dist(p1, path[path.length-1]);
      }

      path.push(p1);

      for(var u = step; u < 1.0; u+=step){
        var p = this.evalcm(p0, p1, p2, p3, u);
        this._len += vec3.dist(p, path[path.length-1]);
        path.push(p);
      }
    }
    this._len += vec3.dist(p2, path[path.length-1]);
    path.push(p2);
    return path;
  };

  /**
  * given a spline with a valid path, normalize will append to each point in path
  * _t attribute that stores the normalized parameter that correspond
  * to that point, according to the lenght of the path.
  * that normalized parameter can be used to interpolate through the spline at constant speed
  */
  MuEngine.Spline.prototype.normalize = function(){
    var acum = 0.0;
    for(var i=0; i<this.path.length; ++i){
      if(i>0){
        acum += vec3.dist(this.path[i], this.path[i-1]);
      }
      this.path[i]._t = acum / this._len;
    }
  };


	/**
	* given a vec3, return the closest t
	*/
	MuEngine.Spline.prototype.getClosestT = function(p){
		var min = Infinity;
		var c = 0;
		for(var i=0; i<this.path.length; ++i){
			var d = vec3.sqrDist(p, this.path[i]);
			if(d < min){
				min = d;
				c = i;
			}
		}
		return this.path[c]._t;
	};


  /**
  * given a parameter betwen 0..1, find the closest normalized pair of points
  * and return the a linear interpolation
  * @param s {number} parameter in the 0..1 range. clamped if not.
  * @param p {vec3} optional output vector. if no present, a new vector is created.
  */
  MuEngine.Spline.prototype.evaluate = function(s, p){
    //clamp..
    s = Math.max(0.0, s);
    s = Math.min(1.0, s);

    //edge cases
    if(s <= 0.0) return this.path[0];
    if(s >= 1.0) return this.path[this.path.length];

    for(var i=0; i<this.path.length-1; ++i){
        var a = this.path[i];
        var b = this.path[i+1];
      if(a._t <= s && b._t >= s){
        var t = (s - a._t)/(b._t - a._t);

        if(!p){
          return vec3.fromValues(
            a[0] + t*(b[0]-a[0]),
            a[1] + t*(b[1]-a[1])
          );
        }else{
          p[0] = a[0] + t*(b[0]-a[0]);
          p[1] = a[1] + t*(b[1]-a[1]);
          return p;
        }

      }
    }


  };


  /**
  * evalulates catmull-rom
  */
  MuEngine.Spline.prototype.evalcm = function(p0, p1, p2, p3, t)
  {
    var p = vec3.create();

    var t0 = ((-t + 2.0) * t - 1.0) * t * 0.5;
    var t1 = (((3.0 * t - 5.0) * t) * t + 2.0) * 0.5;
    var t2 = ((-3.0 * t + 4.0) * t + 1.0) * t * 0.5;
    var t3 = ((t - 1.0) * t * t) * 0.5;

    p[0] = p0[0] * t0 + p1[0] * t1 + p2[0] * t2 + p3[0] * t3;
    p[1] = p[1] * t0 + p1[1] * t1 + p2[1] * t2 + p3[1] * t3;

    return p;
  };



})(window.MuEngine);
