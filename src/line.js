(function(MuEngine){
    'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}

  //------- LINE CLASS -------------------


	/**
	 * Line is a primitive. it holds two points of type Vector3.
	 */
	MuEngine.Line	= function(ori, end,  color){
		this.ori =ori;
	  this.end = end;
		this.color = color || "#cccccc";
 	};

	/*
	 * renders the primitive (line)
	 * @param: node: The node this primitive belongs to
	 */
	MuEngine.Line.prototype.render = function(node, cam){

		vec3.transformMat4(MuEngine.p0, this.ori, node.wm);
		vec3.transformMat4(MuEngine.p1, this.end, node.wm);
		cam.renderLine(MuEngine.p0, MuEngine.p1, this.color);
	};

	/*do nothing*/
	MuEngine.Line.prototype.update = function(node, dt){
	};

})(window.MuEngine);
