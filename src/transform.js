(function(MuEngine){
  'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}

  //------- TRANSFORM CLASS --------

  MuEngine.Transform = function(){
    this._dirty = true;
    this.pos = vec3.create();
    this.rot = quat.create();
    this.mat = mat4.create();
    this.scale = 1.0; //we assume a uniform scale
    this.update();
  };

  /**
	 * call whenever pos or rot changes to update matrix
	 */
  MuEngine.Transform.prototype.update = function(){
    mat4.fromRotationTranslation(this.mat, this.rot, this.pos);

    /*instead of using glmatrix scale method, we implement a direct transform
  to take adventage of the uniform scale feature*/
    this.mat[0] = this.mat[0] * this.scale;
    this.mat[1] = this.mat[1] * this.scale;
    this.mat[2] = this.mat[2] * this.scale;
    this.mat[3] = this.mat[3] * this.scale;
    this.mat[4] = this.mat[4] * this.scale;
    this.mat[5] = this.mat[5] * this.scale;
    this.mat[6] = this.mat[6] * this.scale;
    this.mat[7] = this.mat[7] * this.scale;
    this.mat[8] = this.mat[8] * this.scale;
    this.mat[9] = this.mat[9] * this.scale;
    this.mat[10] = this.mat[10] * this.scale;
    this.mat[11] = this.mat[11] * this.scale;

    this._dirty = false;
  };

  /**
	 * multiply given matrix by this.mat, store result in out
	 */
  MuEngine.Transform.prototype.multiply = function(mat, out){
    if(this._dirty) this.update();
    mat4.multiply(out, mat, this.mat);
  };

  /**
	* multiply a given point for this.mat, store the result in out
    * when used within a node object, it is equivalent to a LOCAL transform!
    * for obtain a GLOBAL transform use the node.multP method.
	*/
  MuEngine.Transform.prototype.multP = function(p, out){
    if(this._dirty) this.update();
    vec3.transformMat4(out, p, this.mat);
  };

  /*
	@deprecated
 * either x,y,z values or a vec3 or a vec2
 */
  MuEngine.Transform.prototype.setPos= function(x, y, z){
		if(arguments.length === 3){
			vec3.set(this.pos, x, y, z);
    }
		else{
      vec3.set(this.pos, x[0], x[1], x.length === 3?x[2]:0);
		}
		this._dirty = true;
  };



  /**
 * rotation over axis Z is the usual rotation used to
 * rotate in 2d. we expect it to be the most used (maybe the unique?)
 * kind of rotation employed in this engine.
 */
  MuEngine.Transform.prototype.setRotZ= function(anglerad){
    quat.identity(this.rot);
    quat.rotateZ(this.rot, this.rot, anglerad);
    this._dirty = true;
  };

  /**
 * rotation over axis Y is used to rotate some 3d objects, like nodes and grids,
 around the vertical axis.
 */
  MuEngine.Transform.prototype.setRotY= function(anglerad){
    quat.identity(this.rot);
    quat.rotateY(this.rot, this.rot, anglerad);
    this._dirty = true;
  };


  /**
* we assume uniform scale
*/
  MuEngine.Transform.prototype.setScale = function(scale){
    this.scale = scale;
    this._dirty = true;
  };


})(window.MuEngine);
