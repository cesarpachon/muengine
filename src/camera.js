(function(MuEngine){
    'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}
//-------- CAMERA CLASS -------------
/**
	 * Camera constructor.
	 * it performs basic perspective transformation.
	 */
MuEngine.Camera = function(){
  //this must be always a normalized vector
  this.eye = vec3.fromValues(0, 0, -1);
  //this.fixed_center = true;
  this.center = vec3.fromValues( 0, 0, 0);
  this.up = vec3.fromValues( 0, 1, 0);
  this.view_mat = mat4.create();
  this.proj_mat = mat4.create();
  this._zoom = 1.0;

  //this is for a projection matrix, but we are going to try first
  //with a ortographic view
  /*this.fovy = Math.PI / 180.0;
		this.aspect = MuEngine.canvas.width / MuEngine.canvas.height;
		this.near = 0.0;
		this.far = 10000.0;
    */
  //orthographic
  this.setOrtho(0, 10, -1, 1, -1, 1);
  //store the view and proj matrix product to avoid constant multiplication of them.
  this.view_proj_mat = mat4.create();
};

/**
  *
  */
MuEngine.Camera.prototype.setOrtho = function(near, far, left, right, bottom, top){
  this.near = near;
  this.far = far;
  this.left = left;
  this.right = right;
  this.top = top;
  this.bottom = bottom;
  this.dirty = true;
};


/**
* in ortho mode, zoom is multiplied by the left, right, bottom and top attributes
* @param  zoom {number} if present, set the zoom. if not, just return the current zoom.
*/
MuEngine.Camera.prototype.zoom = function(zoom){
  if(zoom && zoom > 0){
    this._zoom = zoom;
    this.dirty = true;
  }
  return this._zoom;
};

/**
  *
  */
MuEngine.Camera.prototype.update = function(){

  if(!this.dirty) return;

  //pay attention: in glMatrix, eye is the center of the camera, not the "lookAt" vector..
  vec3.add(MuEngine.p0, this.center, this.eye);
  mat4.lookAt(this.view_mat, this.center, MuEngine.p0, this.up);
  //mat4.perspective(this.proj_mat, this.fovy, this.aspect, this.near, this.far);
  mat4.ortho(this.proj_mat, this.left*this._zoom, this.right*this._zoom, this.bottom*this._zoom, this.top*this._zoom, this.near, this.far);
  mat4.multiply(this.view_proj_mat, this.proj_mat, this.view_mat);
  this.dirty = false;
};


/*
	* transforms a point from world coordinates to eye coordinates
	*/
MuEngine.Camera.prototype.world2eye = function(point, pointout){
  this.update();
  vec3.transformMat4(pointout, point, this.view_proj_mat);
};

/**
	 * given a point in world space, multiply by view_mat and proj_mat and store
	 * result in pointout, in viewport coordinates (pixels)
	 */
MuEngine.Camera.prototype.project = function(point, pointout){
  this.world2eye(point, pointout);
  //transform from normalized device coords to viewport coords..
  pointout[0] = (pointout[0]*MuEngine.canvas.width) + (MuEngine.canvas.width>>1) ;
  pointout[1] = (pointout[1]*MuEngine.canvas.height) + (MuEngine.canvas.height>>1) ;
  //pointout[2] = aux2[2];

  //invert Y!
  pointout[1] = MuEngine.canvas.height - pointout[1];
  //console.log("Camera.project: device: ",aux2,"->",pointout);
};

/**
	 * set the center of the camera at the given point.
     * @param pos a global position
	 */
MuEngine.Camera.prototype.setCenter = function(pos){
  vec3.copy(this.center, pos);
  this.dirty = true;
};

/**
	 * move the center and the eye of the camera using a delta vector
	 */
MuEngine.Camera.prototype.move = function(delta){
  vec3.add(this.center, this.center, delta);
  //no sense! now eye is a vector, not a point! vec3.add(this.eye, this.eye, delta);
  this.dirty = true;
};

/**
     * receives a global point and update the eye vector so it points toward there.
     * eye must always be kept normalized
     * @param pos point in global coords
     */
MuEngine.Camera.prototype.lookAt = function(pos){
  vec3.subtract(this.eye, pos, this.center);
  vec3.normalize(this.eye, this.eye);
  this.dirty = true;
};


/**
     * set the eye direction vector
     * @param dir a normalized vector
     */
MuEngine.Camera.prototype.setEyeDir = function(dir){
  vec3.copy(this.eye, dir);
  this.dirty = true;
};

/**
	* performs frustum culling against a sphere in world coordinates
	* @param center: vec3 in world coordinates
	*	@param radius: radius
	*/
MuEngine.Camera.prototype.containsSphere = function(center, radious){
  //@todo: implement!
  console.log("Camera.containsSphere: not implemented!");
};

/**
	* convenient method to perform culling against sphere, it expects
	* parameters already pre-computed
	* param: center: center in VIEW coordinates.
	* param: radious:
	*/
MuEngine.Camera.prototype.containsSphere2 = function(center, radious){

};

MuEngine.Camera.prototype.log = function(){
  console.log("Camera: center:", this.center, " eye:", this.eye);
};


/**
	* lines are expected in world coordinates
	*/
MuEngine.Camera.prototype.renderLine = function(ori, end, color){
  this.project(ori,MuEngine.p0);
  this.project(end,MuEngine.p1);
  MuEngine.ctx.beginPath();
  MuEngine.ctx.moveTo(MuEngine.p0[0],MuEngine.p0[1]);
  MuEngine.ctx.lineTo(MuEngine.p1[0],MuEngine.p1[1]);
  MuEngine.ctx.closePath();
  MuEngine.ctx.strokeStyle = color;
  MuEngine.ctx.stroke();
};



/**
  * @param points {Array} array of vec3 points with world coordinates
  */
MuEngine.Camera.prototype.renderPolygon = function(points){
  MuEngine.ctx.save();
  MuEngine.ctx.fillStyle = '#f00';
  MuEngine.ctx.beginPath();
  this.project(points[0], MuEngine.p0);
  MuEngine.ctx.moveTo(MuEngine.p0[0], MuEngine.p0[1]);
  for(var i = 1; i<points.length; ++i){
    this.project(points[i], MuEngine.p0);
    MuEngine.ctx.lineTo(MuEngine.p0[0], MuEngine.p0[1]);
  }
  MuEngine.ctx.closePath();
  MuEngine.ctx.fill();
  MuEngine.ctx.restore();
};


/*
     this code is adapted from:
     http://tulrich.com/geekstuff/canvas/jsgl.js
     https://github.com/cesarpachon/pre3d/blob/master/pre3d.js
     refered by dean McNamee
     https://github.com/deanm
    */
MuEngine.Camera.prototype.drawTriangle = function(im, x0, y0, x1, y1, x2, y2,
                                                   sx0, sy0, sx1, sy1, sx2, sy2) {
  MuEngine.ctx.save();

  // Clip the output to the on-screen triangle boundaries.
  MuEngine.ctx.beginPath();
  MuEngine.ctx.moveTo(x0, y0);
  MuEngine.ctx.lineTo(x1, y1);
  MuEngine.ctx.lineTo(x2, y2);
  MuEngine.ctx.closePath();
  //ctx.stroke();//xxxxxxx for wireframe
  MuEngine.ctx.clip();

  /*
     ctx.transform(m11, m12, m21, m22, dx, dy) sets the context transform matrix.

     The context matrix is:

     [ m11 m21 dx ]
     [ m12 m22 dy ]
     [  0   0   1 ]

     Coords are column vectors with a 1 in the z coord, so the transform is:
     x_out = m11 * x + m21 * y + dx;
     y_out = m12 * x + m22 * y + dy;

     From Maxima, these are the transform values that map the source
     coords to the dest coords:

     sy0 (x2 - x1) - sy1 x2 + sy2 x1 + (sy1 - sy2) x0
     [m11 = - -----------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sy1 y2 + sy0 (y1 - y2) - sy2 y1 + (sy2 - sy1) y0
     m12 = -----------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sx0 (x2 - x1) - sx1 x2 + sx2 x1 + (sx1 - sx2) x0
     m21 = -----------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sx1 y2 + sx0 (y1 - y2) - sx2 y1 + (sx2 - sx1) y0
     m22 = - -----------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sx0 (sy2 x1 - sy1 x2) + sy0 (sx1 x2 - sx2 x1) + (sx2 sy1 - sx1 sy2) x0
     dx = ----------------------------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sx0 (sy2 y1 - sy1 y2) + sy0 (sx1 y2 - sx2 y1) + (sx2 sy1 - sx1 sy2) y0
     dy = ----------------------------------------------------------------------]
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0
     */

  // TODO: eliminate common subexpressions.
  var denom = sx0 * (sy2 - sy1) - sx1 * sy2 + sx2 * sy1 + (sx1 - sx2) * sy0;
  if (denom === 0) {
    return;
  }
  var m11 = - (sy0 * (x2 - x1) - sy1 * x2 + sy2 * x1 + (sy1 - sy2) * x0) / denom;
  var m12 = (sy1 * y2 + sy0 * (y1 - y2) - sy2 * y1 + (sy2 - sy1) * y0) / denom;
  var m21 = (sx0 * (x2 - x1) - sx1 * x2 + sx2 * x1 + (sx1 - sx2) * x0) / denom;
  var m22 = - (sx1 * y2 + sx0 * (y1 - y2) - sx2 * y1 + (sx2 - sx1) * y0) / denom;
  var dx = (sx0 * (sy2 * x1 - sy1 * x2) + sy0 * (sx1 * x2 - sx2 * x1) + (sx2 * sy1 - sx1 * sy2) * x0) / denom;
  var dy = (sx0 * (sy2 * y1 - sy1 * y2) + sy0 * (sx1 * y2 - sx2 * y1) + (sx2 * sy1 - sx1 * sy2) * y0) / denom;

  MuEngine.ctx.transform(m11, m12, m21, m22, dx, dy);

  // Draw the whole image.  Transform and clip will map it onto the
  // correct output triangle.
  //
  // TODO: figure out if drawImage goes faster if we specify the rectangle that
  // bounds the source coords.
  MuEngine.ctx.drawImage(im, 0, 0);
  MuEngine.ctx.restore();
};

})(window.MuEngine);
