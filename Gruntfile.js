module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        // define a string to put between each file in the concatenated output
        separator: ''
      },
      dist: {
        // the files to concatenate
        src: [
          'src/header.js',
          'src/transform.js',
          'src/animator.js',
          'src/node.js',
          'src/camera.js',
          'src/cell.js',
          'src/grid.js',
          'src/avatar.js',
          'src/line.js',
          'src/multiline.js',
          'src/imagehandler.js',
          'src/sprite.js',
          'src/infinite_background.js',
          'src/spline.js',
          'src/spline_tracker.js',
          'src/util.js',
          'src/priority_queue.js',
          'src/engine.js'
        ],
        // the location of the resulting JS file
        dest: 'dist/<%= pkg.name %>.js'
      }
    },
    watch: {
      files: ['<%= concat.dist.src %>'],
      tasks: ['make']
    },
    jasmine: {
      test:{
        src :[
          'node_modules/gl-matrix/dist/gl-matrix-min.js',
          'node_modules/howler/howler.js',
          '<%= concat.dist.src %>'
        ],
        options: {
          specs : 'specs/**/*spec.js',
          helpers : 'specs/helpers/*.js',
          timeout : 10000
        }
      }
    },
    jsdoc: {
      dist : {
        src: ['src/*.js'],
        options: {
          destination: 'jsdoc'
        }
      }
    },

    jshint: {
      options:{
        jshintrc:true
      },
      target:{
        src:['<%= concat.dist.src %>']
      }
    },

    copy: {
      dist: {
        nonull: true,
        files: [
          {
            expand: true,
            cwd: 'dist/',
            src: ['<%= pkg.name %>.js'],
            dest: '../osymphonyjs/www/js/',
            filter: 'isFile'
          }
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-jsdoc');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-jshint');


  grunt.registerTask('test', ['jasmine']);
  grunt.registerTask('default', ['watch']);
  grunt.registerTask('make', ['jshint', 'test', 'concat:dist', 'copy:dist']);
  grunt.registerTask('doc', ['jsdoc:dist']);
};
