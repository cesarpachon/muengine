(function(MuEngine){
    'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}


//---------------- class SplineTracker extends Animator ----------------


/**
* makes a node to follow a spline in a given time.
* expects within config:
* - spline: attribute with a MuEngine.Spline object.
* - duration: if set, speed will be computed so it moves through the whole spline in the given time
*/
MuEngine.SplineTracker  = function(config){
	config.target =  MuEngine.Animator.TARGET_POS;
	MuEngine.Animator.call(this, config);
    //in parent constructor, startVal and endVal are passed as references.
    this.spline = config.spline;
  	this.val = vec3.clone(this.spline.path[0]);
};
//chaining prototypes

MuEngine.SplineTracker.prototype  = new MuEngine.Animator();

MuEngine.SplineTracker.prototype.apply = function(node){

  this.spline.evaluate(this.step, this.val);

	//vec3.subtract(this.val, this.endVal, this.startVal);
	//vec3.scale(this.val, this.val, this.step);
  // vec3.add(this.val, this.val, this.startVal);
  node.transform.setPos(this.val[0], this.val[1], this.val[2]);
};


})(window.MuEngine);
