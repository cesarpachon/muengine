(function(MuEngine){
    'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}
//------- AVATAR CLASS EXTENDS NODE ------------

/**
 * an avatar is a special node that is attached to a grid, and provided special animators and callbacks
 * to move through the grid.
 * movement is cell-based: it will move from a cell into another.
 * we assume that the associated primitive support the play anim interface.
 */

/**
 * avatar constructor
 * config object:
 * 	grid: grid this avatar is attached to
 *  row: inicial row
 *  col: inicial col
 *  speed: time required to move from one cell to another (seconds)
 */
MuEngine.Avatar = function(config){
	MuEngine.Node.call(this);
	this.grid = config.grid;
	this.speed = config.speed || 0.1;
	if(!this.grid){
		throw "Avatar.constructor: grid must be defined";
	}
	//the current cell
	this.cell = this.grid.getCell(config.row, config.col);
  this.nextCell = null; //when moving..
  var offset = this.cell.getRandomPos(false, 0.3);
  this.transform.setPos(offset[0],offset[1],offset[2]);
  //this.transform.update();
  this.cell.addChild(this);
  this.moving = false; //false if not moving. "dir" string if moving.
  //used to store mappings between moving directions and animation names. @see mapWalkAnimation
  this.walkanims = {};
  //array of pairs of animname, frequency used for idle behavior. @see addIdleAnimation
  this.idleanims = [];
};

//chaining prototypes
MuEngine.Avatar.prototype = new MuEngine.Node();


/**
 * create an animator that will move the current node from the current cell
 * to the cell pointed by dir.
 * if the target dir does not exist (out of grid boundary) or is not walkable
 * (either for static or dynamic obstacles) the method will return false.
 * in the other hand, if the movement is allowed, it will return true.
 * about the directions:
 * we asume a default view with x axis points to the left and +z points to the screen.
 * then, north is -Z, south is Z, east is +X, east is -X
 * @param dir {string} one of "north", "south", "west", "east".
 * @param cbDone {function} success callback
 * @return {MuEngine.err} OK if can move, MuEngine.err_code if not.
 */
MuEngine.Avatar.prototype.move = function(_dir, cbDone){
    if(!(_dir === "north" || _dir === "south" || _dir === "east" || _dir === "west")){
        throw "invalid_direction";
    }

  //return if already moving
  if(this.moving) return MuEngine.err.INVALID_STATUS;

  //return if there is a wall that prevent movement in the desired dir
  if(this.cell.hasWall(_dir)){
    return MuEngine.err.CELL_UNWALKABLE;
  }

    var col = this.cell.col + ((_dir === "south")?1:((_dir === "north")?-1:0));
    var row = this.cell.row + ((_dir === "east")?1:((_dir === "west")?-1:0));
    this.nextCell = this.grid.getCell(row, col);
    if(!this.nextCell){
        //out of boundaries!
        return MuEngine.err.WORLD_EDGE;
    }
    if(!this.nextCell.isWalkable()){
        this.nextCell = null;
        return MuEngine.err.CELL_UNWALKABLE;
    }
    this.moving = _dir;
    var self = this;

    /* we must transform the coordinates from the cells to the
     * local space of the avatar. */
    var relend = this.nextCell.getRandomPos(false, 0.2);
    var absend = MuEngine.p0; //vec3.create();
    vec3.add(absend, this.nextCell.wp, relend);
    //relend is relative to nextCell.wp, relend2 is relative to current pos of avatar.
    var relend2 = MuEngine.p1; //vec3.create();
    vec3.subtract(relend2, absend, this.wp);
    //duration must be computed from speed!
    var duration = vec3.len(relend2) / (this.speed/1000);

    var relend3 = MuEngine.p2;//vec3.create();
    vec3.add(relend3, this.transform.pos, relend2);


     var animator = new MuEngine.AnimatorPos({
      start: this.transform.pos, // MuEngine.pZero, //
      end: relend3,
      duration: duration,
      loops: 1,
      cb: function(){
        self.cell.removeChild(self);
        self.cell = self.nextCell;
        self.cell.addChild(self);
        self.transform.setPos(relend[0], relend[1], relend[2]);
        self.nextCell = null;
        self.moving = false;
        //play the idle animation..
        var idleanim = self.pickIdleAnimation();
        if(idleanim){
          self.primitive.play(idleanim, true);
        }
        if(cbDone) cbDone();
      }
    });
    this.addAnimator(animator);

    //play sprite animation if available
    if(this.walkanims[_dir]){
      this.primitive.play(this.walkanims[_dir], true);
    }
 return MuEngine.err.OK;
};

/**
 * maps the name of an animation to a specific dir, to play the animation along with the move method.
 * @param animname {string} name of the animation in the animatedsprite associated to this avatar as primitive.
 * @param dir {string} one of 'north', 'south', 'west', 'east'
 */
MuEngine.Avatar.prototype.mapWalkAnimation = function(animname, dir){
  if(dir === "north"){
    this.walkanims.north = animname;
  }
  else if(dir === "south"){
    this.walkanims.south = animname;
  }
  else if(dir === "east"){
    this.walkanims.east = animname;
  }
  else if(dir === "west"){
    this.walkanims.west = animname;
  }
};

/**
 * register a new idle animation in the idleAnimation list.
 * each time the avatar is going to play a idle animation, it will pick a random one from the list.
 * you can also specify a frequency to affect the percentage of use of each anim.
 * @param animname {string} name of the animation
 * @param frequency {number} percentage between 0 and 1
 */
MuEngine.Avatar.prototype.addIdleAnimation = function(animname, frequency){
  this.idleanims.push({
    name: animname,
    frequency: frequency
  });
};

/**
 * randomly picks one of the registered idleanimations, taking into account the frecuency of each one.
 * @return {string} name of the idle animation to play. null if no animations had been registered.
 */
MuEngine.Avatar.prototype.pickIdleAnimation = function(){
  if(!this.idleanims.length){
    return null;
  }
  if(this.idleanims.length === 1){
    return this.idleanims[0].name;
  }
  var r = Math.random();
  var acum = 0;
  for(var i=0; i<this.idleanims.length; ++i){
    var anim = this.idleanims[i];
    acum += anim.frequency;
    if(acum >= r){
      return anim.name;
    }
  }
};

})(window.MuEngine);


