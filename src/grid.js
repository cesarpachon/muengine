(function(MuEngine){
    'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}
  //------- GRID CLASS ------------------

	/**
	 * helper sort function for the render queue of the grid
	 */
	var _compareCellsByEyePos = function(cellA, cellB){
    return cellA.ep[2] < cellB.ep[2]? 1 : cellA.ep[2] > cellB.ep[2]? -1 : 0;
	};

	/**
	 * Grid constructor
	 * width x height
	 * @param width num of rows
	 * @param height num of columns
	 * @param cellsize length of a cell side
	 */
	MuEngine.Grid = function(width, height, cellsize, color){
		var i;
    this.width = width;
		this.height = height;
	 	this.cellsize = cellsize;
		this.color = color || "#cccccc";
		this.cells = new Array(width * height);
		//this.queue = new MuEngine.PriorityQueue(_compareCellsByEyePos);
		this.sorted_cells = new Array(width * height);
		for(i=0; i<this.width; ++i){
				for(var j=0; j< this.height; ++j){
					var cell = new MuEngine.Cell(i, j, cellsize);
					var p = (i*this.height)+j;
					this.cells[p] = cell;
					this.sorted_cells[p] = cell;
				}
		}
	};

	/*
	* Grid static attributes
	*/
	MuEngine.Grid.prototype.p0 = vec3.create();
  MuEngine.Grid.prototype.p1 = vec3.create();


	/**
	 * return a Cell object for the given i,j coordinate.
	 * null if wrong coords.
	 */
	MuEngine.Grid.prototype.getCell = function(i, j){
		if(i < 0 || j < 0 || i >= this.width || j >= this.height)
			return null;
		return this.cells[(i*this.height)+ j];
	};

/**
 *
 * @param {Object} node
 * @param {Object} cam
 */
	MuEngine.Grid.prototype.render = function(node, cam){
		var i, cell;
    this._renderGrid(node, cam);
    for(i=0; i<this.cells.length; ++i){
			cell = this.cells[i];
			cam.world2eye(cell.wp, cell.ep);
      //here its a good point to perform occlusion culling..
			//this.queue.push(cell);
		}
		this.sorted_cells.sort(_compareCellsByEyePos);
		//hopefully, here we have a list of visible cells sorted by depth..
		//var cell = this.queue.pop();
		//while(cell!=null){
	    for(i=0; i<this.sorted_cells.length; ++i){
			cell = this.sorted_cells[i];
			cell.render(node.wm);
			//cell = this.queue.pop();
		}

	};

/**
 *
 * @param {Object} node
 * @param {Object} cam
 */
	MuEngine.Grid.prototype._renderGrid = function(node, cam){
		var w = this.height*this.cellsize;
		//draw rows
		var aux = 0;
		for(var i=0; i<=this.width; ++i){
			vec3.set(this.p0, aux, 0, 0);
			vec3.set(this.p1, aux, 0, w);
			node.multP(this.p0, this.p0);
			node.multP(this.p1, this.p1);
			cam.renderLine(this.p0, this.p1, this.color);
			aux += this.cellsize;
		}
		var h = this.width*this.cellsize;
		aux = 0;
		for(var j=0; j<=this.height; ++j){
			vec3.set(this.p0, 0, 0, aux);
			vec3.set(this.p1, h, 0, aux);
			node.multP(this.p0, this.p0);
			node.multP(this.p1, this.p1);
			cam.renderLine(this.p0, this.p1, this.color);
			aux += this.cellsize;
		}
    //there is some selected cell?
    if(this.selectedCell){
      cam.renderPolygon(this.selectedCell.wcorners);
    }
	};


  /**
    *
    */
  MuEngine.Grid.prototype.update = function(node, dt){
    for(var i=0; i<this.cells.length; i++){
      this.cells[i]._updateCorners(node);
      this.cells[i].update(dt);
    }
  };


  /**
  * return a cell if it is under the given coord. null if not.
  * note: it did not take into account collisions with sprites!
  */
  MuEngine.Grid.prototype.collision = function(x, y, node, cam){
    this.selectedCell = null;
    //@todo: implement some kind of frustum-culling to avoid testing all the cells
     for(var i=0; i<this.cells.length; i++){
      if(this.cells[i].collision(x, y, node, cam)){
        this.selectedCell = this.cells[i];
        return this.selectedCell;
      }
    }
    return this.selectedCell;
  };

})(window.MuEngine);
