(function(MuEngine){
  'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}
  /**
* common utilities.
* added as static methods to MuEngine module
*/

  /**
 * utility method to check for vector equality.
 */
  MuEngine.vec3equ = function(a, b){
    return vec3.squaredDistance(a, b) < 0.0001;
  };


  MuEngine.vec3log = function(label, p){
    console.info("MuEngine.vec3log: "+label+":"+p[0].toFixed(2)+", "+p[1].toFixed(2)+", "+p[2].toFixed(2));
  };

  //calculate the matrix center and dump to console
  MuEngine.mat4centerLog= function(label, mat){
    p = vec3.fromValues(0, 0, 0);
    vec3.transformMat4(p, p, mat);
    console.info("MuEngine.mat4centerLog: "+label+":"+p[0].toFixed(2)+", "+p[1].toFixed(2)+", "+p[2].toFixed(2));
  };

  /**
* utility method
*/
  MuEngine.deg2rad = function(deg){
    return  deg * (Math.PI / 180);
  };


  MuEngine.rad2deg= function(rad){
    return  rad * 180 / Math.PI;
  };


  /**
* check if the given point is inside the rectangle
*/
  MuEngine.pointInAABBox = function(x, y, rx, ry, rw, rh){
    return x >= rx && x <= (rx+rw) && y >= ry && y <= (ry+rh);
  };

  /**
* check if the given point is inside the polygon defined by the other points
* the idea is to check the z-axis of the cross product of each edge with vectors from each
* corner to the given point. if all the signs are equals, it is inside.
*/
  MuEngine.pointInOOBBox = function(x, y, x0, y0, x1, y1, x2, y2, x3, y3){
    /**
  * a is the common point, root of both vectors. b,c are the ends of each vector.
  * return -1 or 1
  */
    function _cross(ax, ay, bx, by, cx, cy){
      vec3.set(MuEngine.p0, bx-ax, by-ay, 0);
      vec3.set(MuEngine.p1, cx-ax, cy-ay, 0);
      vec3.cross(MuEngine.p2, MuEngine.p0, MuEngine.p1);
      return MuEngine.p2[2] > 0?1:-1;
    }
    var c = 0;

    c += _cross(x0, y0, x, y, x1, y1);
    c += _cross(x1, y1, x, y, x2, y2);
    if(Math.abs(c)<2) return false;
    c += _cross(x2, y2, x, y, x3, y3);
    if(Math.abs(c)<3) return false;
    c += _cross(x3, y3, x, y, x0, y0);
    return Math.abs(c) === 4;
  };


  /**
  * just linear interpolation
  */
  MuEngine.interpolate = function(a, b, t){
    return a + t*(b-a);
  };

})(window.MuEngine);
