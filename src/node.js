(function(MuEngine){
	'use strict';

	if(!MuEngine){ MuEngine = window.MuEngine = {};}
	//------- NODE CLASS ------------

	/**
 * Node Constructor
 * implements scene graph.
 * a node has a transform, a primitive and a list of children nodes.
	*/
	MuEngine.Node = function(primitive){
		this.transform = new MuEngine.Transform();
		this.primitive = primitive || null;
		this.children = [ ];
		//world matrix.
		this.wm = mat4.create();
		//world position
		this.wp = vec3.create();
		//eye position
		this.ep = vec3.create();
	};

	MuEngine.Node.prototype.addChild = function(node){
		this.children.push(node);
	};


	MuEngine.Node.prototype.removeChild = function(node){
		for(var i=0; i<this.children.length; ++i){
			if(this.children[i] === node){
				this.children.splice(i, 1);
				return;
			}
		}
	};


	/**
* use the given matrix as parent matrix, compute world transformation using local transform
*/
	MuEngine.Node.prototype.updateWorldMat = function(worldmat){
		this.transform.multiply(worldmat, this.wm);
		vec3.transformMat4(this.wp, MuEngine.pZero, this.wm);
		MuEngine.camera.project(this.wp, this.ep);
	};


	/**
 * recursive function
 */
	MuEngine.Node.prototype.render = function(mat){
		this.updateWorldMat(mat);
		if(this.primitive){
			this.primitive.render(this, MuEngine.camera);
		}
		for(var i=0; i<this.children.length; ++i){
			this.children[i].render(this.wm);
		}
	};


	/**
* animators are temporal objects. they are added on demand, and once they
* reach the finished status, they are removed from the collection.
*/
	MuEngine.Node.prototype.addAnimator= function (animator){
		if(!this.animators){
			this.animators = [];
		}
		this.animators.push(animator);
	};

	/**
	* helper method that invoke pause over all running animators
	*/
	MuEngine.Node.prototype.pause = function(){
		if(!this.animators) return;
		this.animators.forEach(function(animator){
			animator.pause();
		});
	};


	/**
	* helper method that invoke resume over all paused animators
	*/
	MuEngine.Node.prototype.resume = function(){
		if(!this.animators) return;
		this.animators.forEach(function(animator){
			animator.resume();
		});
	};

	/**
*
*/
	MuEngine.Node.prototype.update = function(dt){
		var i;
		if(this.animators){
			for(i=0; i<this.animators.length; ){
				var animator = this.animators[i];
				animator.update(dt, this);
				if(animator.isFinished()){
					this.animators.splice(i, 1);
				}else{
					++i;
				}
			}
		}
		if(this.primitive){
			this.primitive.update(this, dt);
		}
		for(i=0; i<this.children.length; ++i){
			this.children[i].update(dt);
		}
	};


	/**
 * global transform a point. if you want to local transform the point,
 * use node.transform.multP instead.
 * @param p
 * @param out
 */
	MuEngine.Node.prototype.multP = function(p, out){
		//local transform..
		//this.transform.multP(p, out);
		//local to global..
		vec3.transformMat4(out, p, this.wm);
	};
})(window.MuEngine);
