(function(MuEngine){
  'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}
  //------------ SPRITE CLASS ----------

  /**
	* Sprite constructor
	* a sprite is a type of node who will render a bitmap.
	* bitmap is loaded through a ImageHandler.
	* width, height will be taken from picture attributes
	*/
  MuEngine.Sprite = function(path /*,width, height*/){
    this.width = 1.0;
    this.height = 1.0;
    this.path = path;
    this.imghandler = MuEngine.getImageHandler(path);
    //bit flag to control anchor. ORed of ANCHOR_XXX flags.
    this.anchor = 0;
    this.tilex = 0;
    this.tiley = 0;
    this._alpha = 1;

    /*(w,h) and (tilew, tileh) are mutually exclusive set of arguments.
		 if tilew, tileh are present, we asume it is an animated sprite with equally-spaced tiles,
		 and tilex, tiley are assumed the indexes while tilew, tileh constant dimensions in pixels.

		 if(w,h) are present, we assume tilex, tiley are pixel original coordinates, and w, y pixel arbitrary dimensions.
		 use this for map to static tilesets with arbitrary dimensions/positions

		 @see Camera.renderSprite method
		*/
    //this.w = null;
    //this.h = null;
    //this.tilew = null;
    //this.tileh = null;
    this._3d = false; //flag that force the sprite to be rendered in 3d mode
  };

  /*
	* sprite static attributes
	*/

  MuEngine.Sprite.prototype.ANCHOR_HCENTER = 0;
  MuEngine.Sprite.prototype.ANCHOR_VCENTER = 0;
  MuEngine.Sprite.prototype.ANCHOR_CENTER = 0;
  MuEngine.Sprite.prototype.ANCHOR_TOP = 1;
  MuEngine.Sprite.prototype.ANCHOR_BOTTOM = 2;
  MuEngine.Sprite.prototype.ANCHOR_LEFT = 4;
  MuEngine.Sprite.prototype.ANCHOR_RIGHT = 8;


  /**
  * setter/getter for the _alpha property
  */
  MuEngine.Sprite.prototype.alpha = function(val){
    if(arguments.length > 0){
      this._alpha = val;
    }
    return this._alpha;
  };


  /**
  *
  */
  MuEngine.Sprite.prototype.play = function(anim, loop){
    if(!this.anims){
      throw "calling play in sprite without animation data";
    }
    if(!this.animctrl){
      this.animctrl = {};
    }
    this.animctrl.curranim = this.anims[anim];
    this.animctrl.elapsed = 0;
    this.animctrl.loop = loop;
    this.tiley = this.animctrl.curranim.row;
    this.tilex = this.animctrl.curranim.tiles[0];
  };

  /**
	* add a new animation to this sprite
	* name: name of the animation
	* row: row that contains the tiles
	* tiles: array of column indexes
	*/
  MuEngine.Sprite.prototype.addAnimation = function(name, row, tiles, duration ){
    if(!this.anims){
      this.anims = {};
    }
    this.anims[name]= {'row': row, 'tiles': tiles, 'duration': duration};
  };


  /**
  *
  */
  MuEngine.Sprite.prototype.update = function(node, dt){
    if(!this.animctrl) return;
    var anim = this.animctrl.curranim;
    if(!anim) return;
    this.animctrl.elapsed += dt;
    if(this.animctrl.elapsed >= anim.duration){
      if(!this.animctrl.loop){
        this.tilex = anim.tiles[anim.tiles.length-1];
        this.animctrl.curranim = null;
        return;
      }
      else{
        this.animctrl.elapsed = this.animctrl.elapsed % anim.duration;
      }
    }
    this.tilex = anim.tiles[Math.floor((anim.tiles.length-1)*(this.animctrl.elapsed/anim.duration))];
    //console.log("anim x "+ this.tilex + " y "+ this.tiley + " elapsed "+ this.animctrl.elapsed + " duration "+ anim.duration);
  };


  /**
  *
  */
  MuEngine.Sprite.prototype.render = function(node, cam){
    var offx, offy, wpx, wpy;
    var _ctx_dirty = false;

    if (MuEngine.ctx.globalAlpha != this._alpha){
      MuEngine.ctx.save();
      MuEngine.ctx.globalAlpha = this._alpha;
      _ctx_dirty = true;
    }

    var img = this.imghandler.img;
    if(!this._3d){
      //use node.ep instead of this: this.project(node.wp, MuEngine.p0);
      //w, h are in world coords.. transform to pixels:
      wpx = (this.width * node.wm[0]*cam.view_proj_mat[0] * MuEngine.canvas.width) / (cam.right - cam.left);
      wpy = (this.height* node.wm[5]*cam.view_proj_mat[5] * MuEngine.canvas.height) / (cam.top - cam.bottom);
      //how about the anchor?
      offy = ((1 & this.anchor) > 0) ? 0 : (((2 & this.anchor) > 0)? -wpy :-(wpy>>1));
      offx = ((4 & this.anchor) > 0) ? 0 : (((8 & this.anchor) > 0)? -wpx :-(wpx>>1));
      if(this.tilew){
        MuEngine.ctx.drawImage(img,
															 (0.5 + (this.tilex*this.tilew)) << 0, (0.5 + (this.tiley*this.tileh)) << 0,
                               this.tilew, this.tileh,
                               (0.5 + (node.ep[0]+offx)) << 0, (0.5 + (node.ep[1]+offy)) << 0,
                                (0.5 + (wpx)) << 0,  (0.5 + (wpy)) << 0);
      }
      else if(this.w){
        MuEngine.ctx.drawImage(img,
                               this.tilex, this.tiley,
                               (0.5 + this.w) << 0, (0.5 + this.h) << 0,
                               (0.5 + (node.ep[0]+offx)) << 0, (0.5 + (node.ep[1]+offy)) << 0,
                               (0.5 + wpx) << 0, (0.5 + wpy) << 0);
      }
      else{
        MuEngine.ctx.drawImage(img, (0.5 +node.ep[0]+offx)<<0, (0.5+node.ep[1]+offy)<<0,
															  (0.5 + (wpx)) << 0,  (0.5 + (wpy)) << 0);
      }
    }
    else{
      //this is a 3d sprite!

      var w2 =this.width*0.5;
      var h2 = this.height*0.5;

      //compute the projected corners and paint as lines
      //notice that for 2dsprites the anchor computation is done in screen space (pixels) but for 3d it must be done in world space
      offy = ((1 & this.anchor) > 0) ? 0 : (((2 & this.anchor) > 0)? 0:h2);
      offx = ((4 & this.anchor) > 0) ? 0 : (((8 & this.anchor) > 0)? -this.width:-w2);

      //lower left corner:
      vec3.set(MuEngine.p0, offx, offy, 0);
      node.multP(MuEngine.p0, MuEngine.p0);
      cam.project(MuEngine.p0, MuEngine.p0);
      //lower right corner:
      vec3.set(MuEngine.p1, sprite.width+offx, offy, 0);
      node.multP(MuEngine.p1, MuEngine.p1);
      cam.project(MuEngine.p1, MuEngine.p1);
      //upper left corner:
      vec3.set(MuEngine.p2, this.width+offx, this.height+offy, 0);
      node.multP(MuEngine.p2, MuEngine.p2);
      cam.project(MuEngine.p2, MuEngine.p2);
      //upper right corner:
      vec3.set(MuEngine.p3, offx, this.height+offy, 0);
      node.multP(MuEngine.p3, MuEngine.p3);
      cam.project(MuEngine.p3, MuEngine.p3);


      cam.drawTriangle(img,
                       MuEngine.p0[0],MuEngine.p0[1], MuEngine.p1[0],MuEngine.p1[1], MuEngine.p2[0],MuEngine.p2[1],
                       0, img.height, img.width, img.height, img.width, 0);

      cam.drawTriangle(img,
                       MuEngine.p0[0],MuEngine.p0[1], MuEngine.p2[0],MuEngine.p2[1], MuEngine.p3[0],MuEngine.p3[1],
                       0, img.height, img.width, 0, 0, 0);
    }

    /* //for debugging
           MuEngine.ctx.beginPath();
           MuEngine.ctx.moveTo(MuEngine.p0[0],MuEngine.p0[1]);
           MuEngine.ctx.lineTo(MuEngine.p1[0],MuEngine.p1[1]);
           MuEngine.ctx.lineTo(MuEngine.p2[0],MuEngine.p2[1]);
           MuEngine.ctx.lineTo(MuEngine.p3[0],MuEngine.p3[1]);
           MuEngine.ctx.lineTo(MuEngine.p0[0],MuEngine.p0[1]);
           MuEngine.ctx.closePath();
           MuEngine.ctx.strokeStyle =  "#00FFFF";
           MuEngine.ctx.stroke();
           */

    if(_ctx_dirty){
        MuEngine.ctx.restore();
    }

  };


  /**
  * check collision with point in screen coordinates (pixel)
  */
  MuEngine.Sprite.prototype.collision = function(px, py, node, cam){

    var offx, offy;
    if(!sprite._3d){
      //w, h are in world coords.. transform to pixels:
      var wpx = (this.width * node.wm[0]*cam.view_proj_mat[0] * MuEngine.canvas.width) / (cam.right - cam.left);
      var wpy = (this.height* node.wm[5]*cam.view_proj_mat[5] * MuEngine.canvas.height) / (cam.top - cam.bottom);

      //how about the anchor?
      offy = ((1 & this.anchor) > 0) ? 0 : (((2 & this.anchor) > 0)? -wpy :-(wpy>>1));
      offx = ((4 & this.anchor) > 0) ? 0 : (((8 & this.anchor) > 0)? -wpx :-(wpx>>1));

      return MuEngine.pointInAABBox(px, py, node.ep[0]+offx, node.ep[1]+offy, wpx, wpy);

    }
    else{
      //this is a 3d sprite!
      var w2 =this.width*0.5;
      var h2 = this.height*0.5;

      //compute the projected corners and paint as lines
      //notice that for 2dsprites the anchor computation is done in screen space (pixels) but for 3d it must be done in world space
      offy = ((1 & this.anchor) > 0) ? 0 : (((2 & this.anchor) > 0)? 0:h2);
      offx = ((4 & this.anchor) > 0) ? 0 : (((8 & this.anchor) > 0)? -this.width:-w2);

      //lower left corner:
      vec3.set(MuEngine.p0, offx, offy, 0);
      node.multP(MuEngine.p0, MuEngine.p0);
      cam.project(MuEngine.p0, MuEngine.p0);
      //lower right corner:
      vec3.set(MuEngine.p1, sprite.width+offx, offy, 0);
      node.multP(MuEngine.p1, MuEngine.p1);
      cam.project(MuEngine.p1, MuEngine.p1);
      //upper left corner:
      vec3.set(MuEngine.p2, this.width+offx, this.height+offy, 0);
      node.multP(MuEngine.p2, MuEngine.p2);
      cam.project(MuEngine.p2, MuEngine.p2);
      //upper right corner:
      vec3.set(MuEngine.p3, offx, this.height+offy, 0);
      node.multP(MuEngine.p3, MuEngine.p3);
      cam.project(MuEngine.p3, MuEngine.p3);

      return MuEngine.pointInOOBBox(px, py,
                                    MuEngine.p0[0],MuEngine.p0[1],
                                    MuEngine.p1[0],MuEngine.p1[1],
                                    MuEngine.p2[0],MuEngine.p2[1],
                                    MuEngine.p3[0],MuEngine.p3[1]);

    }
  };
})(window.MuEngine);
