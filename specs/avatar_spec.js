describe("Avatar", function() {
  'use strict';

  var getFakeGrid = function(){
    return {
      getCell: function() {
        return {
          addChild: function(){},
          getRandomPos: function(a, b){return [0,0,0];}
        };
        }
      };
  };

  it("pick random anims based on frequency", function() {
    var avatar = new MuEngine.Avatar({grid: getFakeGrid()});
    avatar.addIdleAnimation("0", 0.1);
    avatar.addIdleAnimation("1", 0.3);
    avatar.addIdleAnimation("2", 0.6);

    var counter = [0, 0, 0];

    for(var i=0; i<100; ++i){
      var animname = avatar.pickIdleAnimation();
      counter[animname]=counter[animname]+1;
    }

    expect(counter[2]>counter[1]).toBe(true);
    expect(counter[1]>counter[0]).toBe(true);

  });
});
