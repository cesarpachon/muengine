/**
 MuEngine: accesible multiplayer game engine
 Copyright (C) 2015 cesarpachon@gmail.com

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published
 by the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 contact: http://www.cesarpachon.com

 releases:
 v0.2.3: jun 26 2015
 added property animator with alpha support
 added concurrent and sequence animators
 added alpha support to sprites

 v0.2.2: jun 25 2015
 added spline class based on catmull-rom
 added spline-tracker animation class with tests

 v0.2.0:
 added camera zoom method


 */
(function(MuEngine){
    'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}


  MuEngine.version = "0.2.3";

	//--- INTERNAL ATTRIBUTES ----

	//active grid
	MuEngine.grid = null;

	//active camera
	MuEngine.camera = null;

	//active canvas
	MuEngine.canvas = null;

	//active context
	MuEngine.ctx = null;

	//target fps
	MuEngine.fps = 2;

	//the interval for the event loop
	MuEngine.interval = null;

	MuEngine.start_time = Date.now();

	/**
	 * last result of invoking MuEngine.transformPoint.
	 * it is also used to store the first coord when
	 * invoking MuEngine.transformLine.
	 */
	MuEngine.pt = vec3.create();
	/**
	 * when invoking MuEngine.transformLine, pt will store
	 * the first coord of the line, and pt2 the last one.
	 */
	MuEngine.pt2 = vec3.create();

 //a reusable, read-only (I hope) zero vector.
	MuEngine.pZero = vec3.fromValues(0, 0, 0);

  //a reusable, read-only (I hope) unit vector.
	MuEngine.pOne = vec3.fromValues(1, 1, 1);

  /*
   * static helper points to avoid temporal vector creations
   */
  MuEngine.p0 = vec3.create();
  MuEngine.p1 = vec3.create();
  MuEngine.p2 = vec3.create();
  MuEngine.p3 = vec3.create();


  /**
  * a few error code constants to better describe error conditions when invoking methods
  */
  MuEngine.err ={
    OK:0,
    INVALID_STATUS:1,
    WORLD_EDGE:2,
    CELL_UNWALKABLE: 3
  };

	/**
	* cache of MuEngine.imageHandler.
	*/
	MuEngine.imageHandlers =[];

 /**
  * default image to be used while the imageHandlers are fully loaded.
	* it is initializaded in lazy-load by MuEngine.getImage method.
 */
	MuEngine.defimg = null;

})(window.MuEngine);

