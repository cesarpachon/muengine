describe("spline", function(){

  var control4 = [
    vec3.fromValues(0,0,0),
    vec3.fromValues(1,1,0),
    vec3.fromValues(2,1,0),
    vec3.fromValues(3,0,0)
  ];

  var control7 = [
    vec3.fromValues(0,0,0),
    vec3.fromValues(1,1,0),
    vec3.fromValues(2,1,0),
    vec3.fromValues(3,0,0),
    vec3.fromValues(4,-1,0),
    vec3.fromValues(5,-1,0),
    vec3.fromValues(6,0,0)
  ];


  function expected_path_len(points, sub){
    return ((points.length-1)*sub) + 1;
  }

  describe("smooth", function(){

    it("should return a smoothed array for a single segment", function(){
      var sub = 3;
      var spline = new MuEngine.Spline(control4, sub, 0.01);
      expect(spline.path.length > 0).toBe(true);
      expect(spline.path.length).toBe(expected_path_len(control4, sub));
      expect(spline.len()).toBeGreaterThan(1+2*1.41, 0);
      expect(spline.len()).toBeCloseTo(1+2*1.41, 0);
    });


    it("should return a smoothed array for a set of segments", function(){
      var sub = 3;
      var spline = new MuEngine.Spline(control7, sub, 0.01);
      expect(spline.path.length > 0).toBe(true);
      expect(spline.path.length).toBe(expected_path_len(control7, sub));
    });



  });


  describe("interpolate", function(){

    /*it("should normalize up to 1.0", function(){
      var sub = 3;
      var spline = new MuEngine.Spline(control4, sub, 0.01);

      var acum = 0.0;
      spline.path.forEach(function(p){
        acum += p._t;
      });

      expect(spline.path.length > 0).toBe(true);

    });*/

  });

});
