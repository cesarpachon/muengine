(function(MuEngine){
    'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}

  //-------- IMAGE HANDLER CLASS -----------------

	/**
	 * Image Handler constructor
	 * it will load imgurl, and use MuEngine.defimg while imgurl is fully loaded.
	 * if sizex, sizey are present, at load time internal canvas will be resized to that size.
	 */
	MuEngine.ImageHandler = function(imgurl, sizex, sizey){
		this.img = MuEngine.defimg;
		//start the async loading process..
		var self = this;
		var realimg = new Image();
		realimg.onload = function(){
			console.log("ImageHandler:loaded " + imgurl);
			//old way: just a image object
			//self.img = realimg;
			//new way: using a canvas instead of a image


		  self.img = document.createElement("canvas");

			if(sizex && sizey){
				self.img.width = sizex;
				self.img.height = sizey;
			}else{
				self.img.width = realimg.width;
				self.img.height = realimg.height;
			}

			var _ctx = self.img.getContext('2d');
			_ctx.drawImage(realimg, 0, 0, realimg.width, realimg.height,
										0, 0, self.img.width, self.img.height);
		};
		realimg.src = imgurl;
	};
})(window.MuEngine);

