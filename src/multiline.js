(function(MuEngine){
  'use strict';

  if(!MuEngine){ MuEngine = window.MuEngine = {};}

  //------- LINE CLASS -------------------


  /**
	 * Line is a primitive. it holds and array of points of type Vector3.
	 */
  MuEngine.Multiline	= function(points,  color){
    this.points =points;
    this.color = color || "#cccccc";
  };

  /*
	 * renders the primitive (line)
	 * @param: node: The node this primitive belongs to
	 */
  MuEngine.Multiline.prototype.render = function(node, cam){

    for(var i=0; i<this.points.length-1; ++i){
      vec3.transformMat4(MuEngine.p0, this.points[i], node.wm);
      vec3.transformMat4(MuEngine.p1, this.points[i+1], node.wm);
      cam.renderLine(MuEngine.p0, MuEngine.p1, this.color);
    }

  };

  /*do nothing*/
  MuEngine.Multiline.prototype.update = function(node, dt){
  };

})(window.MuEngine);
