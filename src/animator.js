	(function(MuEngine){
		'use strict';

		if(!MuEngine){ MuEngine = window.MuEngine = {};}

		//-------- ANIMATOR CLASS -----------------

		/**
	 * Animator class constructor
		 parameters as attributes of a config object:
			target, startVal, endVal, type, duration, loops
	 * @param target: one of Animator.prototype.TARGET_XXX
	 * @param type: One of Animator.prototype.TYPE_XXX
	 * @param  loops
	 * N > 0: Number of executions
	 * N <= 0: infinite loop
	 */
		MuEngine.Animator  = function(config){
			//prototype chaining will call this method with empty parameters
			if(config === undefined) return;
			//configuration parameters
			//this.target = config.target ||  "pos";
			this.loops = (typeof config.loops === "number")? config.loops : 1;
			this.startVal = (typeof config.start !== "undefined")? config.start : 0.0;
			this.endVal = (typeof config.end !== "undefined")? config.end: 1.0;
			this.duration = (typeof config.duration === "number")? config.duration: 1000;
			this.cb = config.cb;

			//internal status variables
			this.elapsedtime = 0;
			this.status = "idle";
			this.step = 0.0;
		};

		MuEngine.Animator.STATUS_IDLE = "idle";
		MuEngine.Animator.STATUS_RUNNING = "running";
		MuEngine.Animator.STATUS_PAUSED = "paused";
		MuEngine.Animator.STATUS_FINISHED = "finished";

	/**
	*
	*/
		MuEngine.Animator.prototype.isFinished = function(){
			return this.status === MuEngine.Animator.STATUS_FINISHED;
		};

		/**
		* move to pause status only if running
		*/
		MuEngine.Animator.prototype.pause = function(){
			if(this.status === MuEngine.Animator.STATUS_RUNNING){
				this.status = MuEngine.Animator.STATUS_PAUSED;
			}
		};

	/**
		* move to running status only if paused
		*/
		MuEngine.Animator.prototype.resume = function(){
			if(this.status === MuEngine.Animator.STATUS_PAUSED){
				this.status = MuEngine.Animator.STATUS_RUNNING;
			}
		};

		/**
	* default implementation with simple linear interpolation.
	* extended animators that did not override this method should implement
	* apply in order to update the node with the value of this.step.
	*/
		MuEngine.Animator.prototype.update = function(dt, node){
			if(this.status === MuEngine.Animator.STATUS_IDLE){
				this.status = MuEngine.Animator.STATUS_RUNNING;
				this.elapsedtime= 0;
				this.step = 0;
				this.apply(node);
			}
			else if(this.status === MuEngine.Animator.STATUS_RUNNING){
				this.elapsedtime  += dt;
				if(this.elapsedtime >= this.duration){
					if(this.loops > 0){
						this.loops--;
						this.step = 1.0;
						this.status = MuEngine.Animator.STATUS_FINISHED;
						this.apply(node);
						if(this.cb) this.cb();
					}else{
						//keep looping..
						this.elapsedtime = this.elapsedtime - this.duration;
						this.step = this.elapsedtime/this.duration;
					}
				}else{
					this.step = this.elapsedtime/this.duration;
				}
				this.apply(node);
			}
			else if(this.status === MuEngine.Animator.STATUS_PAUSED){
				//just ignore?
			}
			else if(this.status === MuEngine.Animator.STATUS_FINISHED){
			}
			else throw "unknown animator status: " + this.status;
		};



		//---------------- class AnimatorPos extends Animator ----------------


		MuEngine.AnimatorPos  = function(config){
			//config.target =  MuEngine.Animator.TARGET_POS;
			MuEngine.Animator.call(this, config);
			//in parent constructor, startVal and endVal are passed as references.
			this.startVal = vec3.clone(this.startVal);
			this.endVal = vec3.clone(this.endVal);
			//this is the "current value" that we are going to keep changing
			this.val = vec3.clone(this.startVal);
		};
		//chaining prototypes

		MuEngine.AnimatorPos.prototype  = new MuEngine.Animator();

		MuEngine.AnimatorPos.prototype.apply = function(node){
			vec3.subtract(this.val, this.endVal, this.startVal);
			vec3.scale(this.val, this.val, this.step);
			vec3.add(this.val, this.val, this.startVal);
			node.transform.setPos(this.val[0], this.val[1], this.val[2]);
		};

		//---------------- class AnimatorRotY extends Animator ----------------
		MuEngine.AnimatorRotY  = function(config){
			MuEngine.Animator.call(this, config);
			this.val = this.startVal;
		};

		//chaining prototypes
		MuEngine.AnimatorRotY.prototype = new MuEngine.Animator();

		MuEngine.AnimatorRotY.prototype.apply = function(node){
			this.val = MuEngine.interpolate(this.startVal, this.endVal, this.step);//this.startVal + this.step*(this.endVal - this.startVal);
			node.transform.setRotY(this.val);
		};

		//---------------- class Animatorproperty extends Animator ----------------
		/*
		* list of valid properties:
		* -alpha: assumes the node has a sprite as primitive and will affect its alpha value
		*/
		MuEngine.AnimatorProperty  = function(config){
			MuEngine.Animator.call(this, config);
			this.val = this.startVal;
			this.property = config.property;
		};

		//chaining prototypes
		MuEngine.AnimatorProperty.prototype = new MuEngine.Animator();

		MuEngine.AnimatorProperty.prototype.apply = function(node){
			this.val = MuEngine.interpolate(this.startVal, this.endVal, this.step);
			if(this.property === "alpha"){
				node.primitive.alpha(this.val);
			}
		};



		//---------------- class AnimatorConcurrent extends Animator -------------
		MuEngine.AnimatorConcurrent  = function(config){
			MuEngine.Animator.call(this, config);
			this.children = config.children; //an array of animators
			if(!this.children || this.children.lenght === 0){
				this.status = MuEngine.Animator.STATUS_FINISHED;
			}
		};

		//chaining prototypes
		MuEngine.AnimatorConcurrent.prototype = new MuEngine.Animator();


		MuEngine.AnimatorConcurrent.prototype.update = function(dt, node){
			if(this.status === MuEngine.Animator.STATUS_FINISHED){
				return;
			}else if(this.status === MuEngine.Animator.STATUS_IDLE){
				this.status = MuEngine.Animator.STATUS_RUNNING;
			}
			else if(this.status === MuEngine.Animator.STATUS_PAUSED){
				return;
			}

			var running = false;
			this.children.forEach(function(child){
				child.update(dt, node);
				if(child.status !== MuEngine.Animator.STATUS_FINISHED){
					running = true;
				}
			});
			if(!running){
				this.status = MuEngine.Animator.STATUS_FINISHED;
			}
		};



		//---------------- class AnimatorSequential extends Animator -------------
		MuEngine.AnimatorSequential  = function(config){
			MuEngine.Animator.call(this, config);
			this.children = config.children; //an array of animators
			if(!this.children || this.children.lenght === 0){
				this.status = MuEngine.Animator.STATUS_FINISHED;
			}
		};

		//chaining prototypes
		MuEngine.AnimatorSequential.prototype = new MuEngine.Animator();


		MuEngine.AnimatorSequential.prototype.update = function(dt, node){
			if(this.status === MuEngine.Animator.STATUS_FINISHED){
				return;
			}
			else if(this.status === MuEngine.Animator.STATUS_IDLE){
				this.status = MuEngine.Animator.STATUS_RUNNING;
				this.active_child = this.children.shift();
			}
			else if(this.status === MuEngine.Animator.STATUS_PAUSED){
				return;
			}


			this.active_child.update(dt, node);
			if(this.active_child.status === MuEngine.Animator.STATUS_FINISHED){
				if(this.children.length > 0){
					this.active_child = this.children.shift();
				}else{
					this.active_child = null;
					this.status = MuEngine.Animator.STATUS_FINISHED;
				}
			}
		};


	})(window.MuEngine);
