describe("collisions", function(){

  describe("AABBox collisions", function(){

    it("should detect collisions", function(){
        expect(MuEngine.pointInAABBox(100, 100, 90, 90, 20, 20)).toBe(true);
    });

    it("should not detect collisions", function(){
        expect(MuEngine.pointInAABBox(10, 10, 90, 90, 20, 20)).toBe(false);
    });


  });

  describe("OOBBox collisions", function(){
    it("should detect collisions cw", function(){
        expect(MuEngine.pointInOOBBox(50, 50, 50, 0, 100, 50, 50, 100, 0, 50)).toBe(true);
    });

    it("should detect collisions ccw", function(){
        expect(MuEngine.pointInOOBBox(50, 50, 50, 0, 0, 50, 50, 100, 100, 50)).toBe(true);
    });

    it("should not detect collisions cw", function(){
        expect(MuEngine.pointInOOBBox(10, 10, 50, 0, 100, 50, 50, 100, 0, 50)).toBe(false);
        expect(MuEngine.pointInOOBBox(10, 90, 50, 0, 100, 50, 50, 100, 0, 50)).toBe(false);
        expect(MuEngine.pointInOOBBox(90, 10, 50, 0, 100, 50, 50, 100, 0, 50)).toBe(false);
        expect(MuEngine.pointInOOBBox(90, 90, 50, 0, 100, 50, 50, 100, 0, 50)).toBe(false);
    });

    it("should not detect collisions ccw", function(){
        expect(MuEngine.pointInOOBBox(10, 10, 50, 0, 0, 50, 50, 100, 100, 50)).toBe(false);
        expect(MuEngine.pointInOOBBox(10, 90, 50, 0, 0, 50, 50, 100, 100, 50)).toBe(false);
        expect(MuEngine.pointInOOBBox(90, 10, 50, 0, 0, 50, 50, 100, 100, 50)).toBe(false);
        expect(MuEngine.pointInOOBBox(90, 90, 50, 0, 0, 50, 50, 100, 100, 50)).toBe(false);
    });


  });

});
