describe("A cell", function() {
  'use strict';
  var cell = null;
  var i = 0;
  var j = 0;
  var cellsize = 10;

  beforeEach(function(){
    cell = new MuEngine.Cell(i, j, cellsize);

  });

  it("generates valid random values", function() {
    var padding = 0.3;
    var point = cell.getRandomPos(false, padding);
    var min = cellsize*padding;
    var max = cellsize*(1-padding);
    expect(point[0]>=min && point[0] <= max).toBe(true);
    expect(point[2]>=min && point[2] <= max).toBe(true);
  });

  it("validates walkability", function(){
    expect(cell.isWalkable()).toBe(true);
    cell.setWalkable(false);
    expect(cell.isWalkable()).toBe(false);
  });


  it("validates walls", function(){
    expect(cell.hasWall("north")).toBe(false);
    expect(cell.hasWall("south")).toBe(false);
    expect(cell.hasWall("east")).toBe(false);
    expect(cell.hasWall("west")).toBe(false);

    cell.setWall("north");
    expect(cell.hasWall("north")).toBe(true);
    expect(cell.hasWall("south")).toBe(false);
    expect(cell.hasWall("east")).toBe(false);
    expect(cell.hasWall("west")).toBe(false);

    cell.setWall("south");
    expect(cell.hasWall("north")).toBe(true);
    expect(cell.hasWall("south")).toBe(true);
    expect(cell.hasWall("east")).toBe(false);
    expect(cell.hasWall("west")).toBe(false);

    cell.setWall("east");
    expect(cell.hasWall("north")).toBe(true);
    expect(cell.hasWall("south")).toBe(true);
    expect(cell.hasWall("east")).toBe(true);
    expect(cell.hasWall("west")).toBe(false);

    cell.setWall("west");
    expect(cell.hasWall("north")).toBe(true);
    expect(cell.hasWall("south")).toBe(true);
    expect(cell.hasWall("east")).toBe(true);
    expect(cell.hasWall("west")).toBe(true);


  });

});
